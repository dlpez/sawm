CREATE TABLE cliente (
    id serial PRIMARY KEY,
    tipo_rif character(1) NOT NULL,
    rif character varying(50) NOT NULL,
    direccion text NOT NULL,
    razon_social character varying(255) NOT NULL,
    nombre character varying(255),
    usuario bigint,
    ip character varying(20)
);
