<?php
namespace Customer;

return array (
		'controllers' => array (
				'invokables' => array (
						'Customer\Controller\Client' => 'Customer\Controller\ClientController',
						'Customer\Controller\Contract' => 'Customer\Controller\ContractController',
				) 
		),
		
		// The following section is new and should be added to your file
		'router' => array (
				'routes' => array (
						'client' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/client[/][:action][/:id]',
										'constraints' => array (
												'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
												'id' => '[0-9]+' 
										),
										'defaults' => array (
												'controller' => 'Customer\Controller\Client',
												'action' => 'index' 
										) 
								) 
						),
						'contract' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/contract[/][:action][/:id]',
										'constraints' => array (
												'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
												'id' => '[0-9]+' 
										),
										'defaults' => array (
												'controller' => 'Customer\Controller\Contract',
												'action' => 'index' 
										) 
								) 
						) 
				) 
		),
		
		'view_manager' => array (
				'template_path_stack' => array (
						'album' => __DIR__ . '/../view' 
				) 
		),

		'doctrine' => array(
	        'driver' => array(
	            __NAMESPACE__ . '_driver' => array(
	                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
	                'cache' => 'array',
	                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
	            ),
	            'orm_default' => array(
	                'drivers' => array(
	                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
	                )
	            )
	        )
	    ),
);