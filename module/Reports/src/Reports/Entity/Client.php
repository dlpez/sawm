<?php
namespace Customer\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\Input;

/**
 * Client Class
 *
 * @ORM\Entity
 * @ORM\Table(name="Client")
 * @property string $artist
 * @property string $title
 * @property int $id
 * @author David López <dlopez@sistelnet.com.ve>
 */
class Client implements InputFilterAwareInterface{
	const TYPE_RIF_V = 'V';
	const TYPE_RIF_J = 'J';
	const TYPE_RIG_G = 'G';
	const TYPE_RIF_E = 'E';
	
	/**
	 * Status for a new client
	 * @const integer
	 */
	const STATUS_CLIENT_NEW = 0;
	
	/**
	 * Status for active client
	 * @const integer
	 */
	const STATUS_CLIENT_ACTIVE = 1;
	
	/**
	 * Status for unactive client
	 * @const integer
	 */
	const STATUS_CLIENT_UNACTIVE = 2;

	/**
	 * Filter for var
	 * @var Zend\InputFilter\Input
	 */
	protected $inputFilter;
	
	/**
	 * Client Id's
	 * @ORM\Id @ORM\Column(type="integer") @ORM\GeneratedValue
	 * @var int
	 */
	protected $id;
	
	/**
	 * Fiscal name for a client
	 * @ORM\Column(type="string")
	 * @var string
	 */
	protected $fiscalName;
	
	/**
	 * Client name's
	 * @ORM\Column(type="string")
	 * @var string
	 */
	protected $name;
	
	/**
	 * Client Rif's
	 * @ORM\Column(type="string")
	 * @var string
	 */
	protected $rif;
	
	/**
	 * Type of Rif
	 * @ORM\Column(type="string")
	 * @var string
	 */
	protected $typeRif;
	
	/**
	 * Client address's
	 * @ORM\Column(type="string")
	 * @var string
	 */
	protected $address;

	/**
	 * Client has contracts
	 * @ORM\OneToMany(targetEntity="Contract", mappedBy="id")
	 * @var Contract[]
	 */
	protected $contracts;
	
	/**
	 * Client Class's contructor
	 *
	 * @param array $data        	
	 */
	public function __construct(array $data = null) {
		if (!is_null($data)) {
			$this->exchangeArray($data);
		}
		$this->contracts = new ArrayCollection();
	}
	
	/**
	 * Magic method for getters
	 *
	 * @param string $name        	
	 * @throws Exception
	 */
	public function __get($name) {
		$method = 'get' . ucfirst ( $name );
		
		if (('mapper' == $name) || ! method_exists ($this, $method)) {
			throw new Exception ('Property Invalid ' . $name);
		}
		return $this->$method();
	}
	
	/**
	 * Magic method for setter
	 *
	 * @param string $name Property name
	 * @param mixed $value Property value
	 */
	public function __set($name, $value) {
		$method = 'set' . ucfirst ( $name );
		if (('mapper' == $name) || ! method_exists ( $this, $method )) {
			throw new \Exception ( 'Property Invalid ' . $name );
		}
		$this->$method($value);
	}
	
	/**
	 * Given an array exchanges data with property
	 *
	 * @param array $data Associative array with property     	
	 * @return void
	 */
	public function exchangeArray($data) {
		$this->id = (! empty ( $data ['id'] )) ? $data ['id'] : null;
		$this->fiscalName = (! empty ( $data ['fiscalName'] )) ? $data ['fiscalName'] : null;
		$this->rif = (! empty ( $data ['rif'] )) ? $data ['rif'] : null;
		$this->address = (! empty ( $data ['address'] )) ? $data ['address'] : null;
		$this->name = (! empty ( $data ['name'] )) ? $data ['name'] : null;
		$this->typeRif = (! empty ( $data ['typeRif'] )) ? $data ['typeRif'] : null;
	}
	
	/**
	 * Get id client's
	 * 
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}
	
	/**
	 * Set id client's
	 *
	 * @param int $id
	 * @return void
	 * @throws Exception If property is invalid
	 */
	public function setId($id) {
		$inputFilId = $this->getInputFilter()->get('id');
		$data = array (
			'id' => $id 
		);
		$inputFilId->setData($data);
		if (! $inputFilId->isValid ())
			throw new \Exception ( "Invalid data for Client Id" );
		$this->id = $inputFilId->getValue ('id');
	}
	
	/**
	 * Get Rif Client's
	 *
	 * @return string
	 */
	public function getRif() {
		return $this->rif;
	}
	
	/**
	 * Set rif client's
	 *
	 * @param string $rif
	 * @return void
	 */
	public function setRif($rif) {
		$this->rif = $rif;
	}
	
	/**
	 * Get type of rif
	 *
	 * @return string
	 */
	public function getTypeRif() {
		return $this->typeRif;
	}
	
	/**
	 * Set type of rif
	 *
	 * @param string $typeRif
	 * @return void
	 */
	public function setTypeRif($typeRif) {
		$this->typeRif = $typeRif;
	}
	
	/**
	 * Get name client's
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}
	
	/**
	 * Set name client's
	 *
	 * @param string $name        	
	 * @return void
	 */
	public function setName($name) {
		$this->name = $name;
	}
	
	/**
	 * Get address client's
	 *
	 * @return string
	 */
	public function getAddress() {
		return $this->address;
	}
	
	/**
	 * Set address client's
	 *
	 * @param string $address
	 * @return void
	 */
	public function setAddress($address) {
		$this->address = $address;
	}
	
	/**
	 * Get fiscal name client's
	 *
	 * @return string
	 */
	public function getFiscalName() {
		return $this->fiscalName;
	}
	
	/**
	 * Set fiscal name client's
	 *
	 * @param string $nameFiscal
	 * @return void
	 */
	public function setFiscalName($fiscalName) {
		$this->fiscalName = $fiscalName;
	}

	/**
	 * Add a contract to client
	 * @param Contract $contract
	 */
	public function addContract(Contract $contract){
		$this->contracts->add($contract);
	}

	/**
	 * Get all contract for this client
	 * @return ArrayColletion
	 */
	public function getContracts(){
		return $this->contracts;
	}
	
	/**
	 *
	 * @param InputFilterInterface $inputFilter        	
	 * @throws \Exception
	 */
	public function setInputFilter(InputFilterInterface $inputFilter) {
		throw new \Exception ( "Not used" );
	}
	
	/**
	 * (non-PHPdoc)
	 *
	 * @see \Zend\InputFilter\InputFilterAwareInterface::getInputFilter()
	 * @return InputFilter
	 */
	public function getInputFilter() {
		if (! $this->inputFilter) {
			$inputFilter = new InputFilter ();
			
			$inputFilter->add ( array (
					'name' => 'id',
					'filters' => array (
							array (
									'name' => 'Int' 
							) 
					),
					'validators' => array (
							array (
									'name' => 'Int' 
							) 
					) 
			) );
			
			$inputFilter->add ( array (
					'name' => 'name',
					'required' => true,
					'filters' => array (
							array (
									'name' => 'StripTags' 
							),
							array (
									'name' => 'StringTrim' 
							) 
					),
					'validators' => array (
							array (
									'name' => 'StringLength',
									'options' => array (
											'encoding' => 'UTF-8',
											'min' => 1,
											'max' => 100 
									) 
							) 
					) 
			) );
			
			$inputFilter->add ( array (
					'name' => 'rif',
					'required' => true,
					'filters' => array (
							array (
									'name' => 'StripTags' 
							),
							array (
									'name' => 'StringTrim' 
							) 
					),
					'validators' => array (
							array (
									'name' => 'StringLength',
									'options' => array (
											'encoding' => 'UTF-8',
											'min' => 1,
											'max' => 100 
									) 
							) 
					) 
			) );
			
			$inputFilter->add ( array (
					'name' => 'fiscalName',
					'required' => true,
					'filters' => array (
							array (
									'name' => 'StripTags' 
							),
							array (
									'name' => 'StringTrim' 
							) 
					),
					'validators' => array (
							array (
									'name' => 'StringLength',
									'options' => array (
											'encoding' => 'UTF-8',
											'min' => 1,
											'max' => 100 
									) 
							) 
					) 
			) );
			
			$inputFilter->add ( array (
					'name' => 'address',
					'required' => true,
					'filters' => array (
							array (
									'name' => 'StripTags' 
							),
							array (
									'name' => 'StringTrim' 
							) 
					),
					'validators' => array (
							array (
									'name' => 'StringLength',
									'options' => array (
											'encoding' => 'UTF-8',
											'min' => 1,
											'max' => 100 
									) 
							) 
					) 
			) );
			
			$this->inputFilter = $inputFilter;
		}
		
		return $this->inputFilter;
	}
	/**
	 * Devuelve todos las claves de la variables constantes de la clase
	 *
	 * @static
	 * @return array
	 */
	static function getAllByConstant($token = null) {
		if (is_null ( $token ) === true) {
			return null;
		} elseif (is_string ( $token ) === true) {
			$token = strtoupper ( $token );
			if (strpos ( $token, '_' ) === false) {
				$token .= "_";
			}
			$oClass = new \ReflectionClass ('Customer\Entity\Client');
			$status = $oClass->getConstants ();
			$estatus = array ();
			foreach ( $status as $key => $value ) {
				if (strpos ( $key, $token ) !== false) {
					$estatus [$value] = str_replace ( "_", " ", substr ( $key, strlen ( $token ) ) );
					strtok ( $key, $token );
				}
			}
			return $estatus;
		} else {
			throw new \Exception ( "It expectd a string" );
		}
	}
	
	/**
	 * Devuelve el nombre de la constante solicitada
	 *
	 * @static
	 *
	 * @param $token Tipo
	 *        	de constante que se quiere
	 * @param $key Valor
	 *        	de la constante
	 * @return array
	 */
	static function getNombreConstant($token, $key) {
		$constantes = Clientes_Model_Cliente::getAllByConstant ( $token );
		return $constantes [$key];
	}
}