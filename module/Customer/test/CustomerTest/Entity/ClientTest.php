<?php 
/**
 * Copyright (c) 2010-2014, Soluciones Tecnologicas y Teleinformaticas Netware C.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Sistelnet C.A. nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 * @package    Customer
 * @author     David López <dlopez@sistelnet.com.ve>
 * @copyright  2010-2014 Sistelnet C.A. <dlopez@sistelnet.com.ve>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://www.sistelnet.com.ve
 */
namespace CustomerTest\Entity;

use Customer\Entity\Client;

class ClientTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * Objeto a probar
	 */
	protected $client;
	
	public function setUp()
	{
		$this->client = new Client();
	}
	
	public function testClientInitialEmpty()
	{
		$this->assertNull($this->client->id);
		$this->assertNull($this->client->firstName);
		$this->assertNull($this->client->lastName);
		$this->assertNull($this->client->address);
		$this->assertNull($this->client->phoneWork);
		$this->assertNull($this->client->phoneCel);
		$this->assertNull($this->client->phoneHome);
		$this->assertNull($this->client->rif);
		$this->assertNull($this->client->fiscalName);
		$this->assertNull($this->client->companyName);
		$this->assertNull($this->client->email);
		$this->assertNull($this->client->status);
		$this->assertNull($this->client->tsCreate);		
		$this->assertNull($this->client->tsUpdate);
	}

	/**
	 * @depends testClientInitialEmpty
	 * @dataProvider goodData
	 */
	public function testLoadGoodData($params){
		$this->client->setOptions($params);

		$this->assertEquals($params['id'], $this->client->id);
		$this->assertEquals(strtoupper($params['firstName']), $this->client->firstName);
		$this->assertEquals(strtoupper($params['lastName']), $this->client->lastName);
		$this->assertEquals(strtoupper($params['address']), $this->client->address);
		$this->assertEquals($params['phoneWork'], $this->client->phoneWork);
		$this->assertEquals($params['phoneCel'], $this->client->phoneCel);
		$this->assertEquals($params['phoneHome'], $this->client->phoneHome);
		$this->assertEquals(strtoupper($params['rif']), $this->client->rif);
		$this->assertEquals(strtoupper($params['fiscalName']), $this->client->fiscalName);
		$this->assertEquals(strtoupper($params['companyName']), $this->client->companyName);
		$this->assertEquals(strtoupper($params['email']), $this->client->email);
		$this->assertEquals($params['status'], $this->client->status);
	}

	/**
	 * @expectedException InvalidArgumentException
	 * @dataProvider badData
	 */
	public function testLoadBadData($params){
		$this->client->setOptions($params);
	}


	public function goodData()
	{
		return
			array(
				array(
					array(
						'id' 		    => '1',
						'firstName'	    => 'David',
						'lastName'		=> 'Lopez',
						'address'	    => 'algun lugar',
						'phoneWork'	    => '02763821423',
						'phoneCel'	    => '02763821423',
						'phoneHome'	    => '02763821423',
						'rif'	        => 'v17369189-9',
						'fiscalName'    => 'sistelnet',
						'companyName'   => 'sistelne c.a.',
						'email'			=> 'dlopez@sistelnet.com.ve',
						'status'		=> '1',
						'tsCreate'		=> '1999-01-08 04:05:06'
					),
				),
				array(				
					array(
						'id' 		    => '1',
						'firstName'	    => str_repeat('x', 255),
						'lastName'		=> str_repeat('x', 255),
						'address'	    => str_repeat('x', 500),
						'phoneWork'	    => str_repeat('2', 255),
						'phoneCel'	    => str_repeat('2', 255),
						'phoneHome'	    => str_repeat('2', 255),
						'rif'	        => str_repeat('x', 255),
						'fiscalName'    => str_repeat('x', 255),
						'companyName'   => str_repeat('x', 255),
						'email'			=> str_repeat('x', 64) . '@g.com',
						'status'		=> 1,
						'tsUpdate'		=> '1999-01-08 04:05:06'
					),
				),
				array(				
					array(
						'id' 		    => '1',
						'firstName'	    => str_repeat('x', 255),
						'lastName'		=> str_repeat('x', 255),
						'address'	    => str_repeat('x', 500),
						'phoneWork'	    => str_repeat('2', 255),
						'phoneCel'	    => str_repeat('2', 255),
						'phoneHome'	    => str_repeat('2', 255),
						'rif'	        => str_repeat('x', 255),
						'fiscalName'    => str_repeat('x', 255),
						'companyName'   => "",
						'email'			=> str_repeat('x', 64) . '@g.com',
						'status'		=> 1
					),
				),
				array(				
					array(
						'id' 		    => '1',
						'firstName'	    => str_repeat('x', 255),
						'lastName'		=> str_repeat('x', 255),
						'address'	    => str_repeat('x', 500),
						'phoneWork'	    => str_repeat('2', 255),
						'phoneCel'	    => str_repeat('2', 255),
						'phoneHome'	    => str_repeat('2', 255),
						'rif'	        => str_repeat('x', 255),
						'fiscalName'    => str_repeat('x', 255),
						'companyName'   => "",
						'email'			=> "b@g.net",
						'status'		=> "1"
					),
				),
			);
	}

	public function badData()
	{
		return
			array(
				array(
					array(
						'id' 		    => 'aaa',
						'firstName'	    => 'David',
						'lastName'		=> 'Lopez',
						'address'	    => 'algun lugar',
						'phoneWork'	    => '02763821423',
						'phoneCel'	    => '02763821423',
						'phoneHome'	    => '02763821423',
						'rif'	        => 'v17369189-9',
						'fiscalName'    => 'sistelnet',
						'companyName'   => 'sistelne c.a.',
						'email'			=> 'dlopez@sistelnet.com.ve',
						'status'		=> '1'
					),
				),
				array(				
					array(
						'id' 		    => '1',
						'firstName'	    => str_repeat('x', 300),
						'lastName'		=> str_repeat('x', 255),
						'address'	    => str_repeat('x', 500),
						'phoneWork'	    => str_repeat('2', 255),
						'phoneCel'	    => str_repeat('2', 255),
						'phoneHome'	    => str_repeat('2', 255),
						'rif'	        => str_repeat('x', 255),
						'fiscalName'    => str_repeat('x', 255),
						'companyName'   => str_repeat('x', 255),
						'email'			=> str_repeat('x', 64) . '@g.com',
						'status'		=> 1
					),
				),				
				array(				
					array(
						'id' 		    => '1',
						'firstName'	    => str_repeat('x', 255),
						'lastName'		=> str_repeat('x', 300),
						'address'	    => str_repeat('x', 500),
						'phoneWork'	    => str_repeat('2', 255),
						'phoneCel'	    => str_repeat('2', 255),
						'phoneHome'	    => str_repeat('2', 255),
						'rif'	        => str_repeat('x', 255),
						'fiscalName'    => str_repeat('x', 255),
						'companyName'   => str_repeat('x', 255),
						'email'			=> str_repeat('x', 64) . '@g.com',
						'status'		=> 1
					),
				),				
				array(				
					array(
						'id' 		    => '1',
						'firstName'	    => str_repeat('x', 255),
						'lastName'		=> str_repeat('x', 255),
						'address'	    => null,
						'phoneWork'	    => str_repeat('2', 255),
						'phoneCel'	    => str_repeat('2', 255),
						'phoneHome'	    => str_repeat('2', 255),
						'rif'	        => str_repeat('x', 255),
						'fiscalName'    => str_repeat('x', 255),
						'companyName'   => str_repeat('x', 255),
						'email'			=> str_repeat('x', 64) . '@g.com',
						'status'		=> 1
					),
				),				
				array(				
					array(
						'id' 		    => '1',
						'firstName'	    => str_repeat('x', 255),
						'lastName'		=> str_repeat('x', 255),
						'address'	    => str_repeat('x', 500),
						'phoneWork'	    => str_repeat('a', 255),
						'phoneCel'	    => str_repeat('2', 255),
						'phoneHome'	    => str_repeat('2', 255),
						'rif'	        => str_repeat('x', 255),
						'fiscalName'    => str_repeat('x', 255),
						'companyName'   => str_repeat('x', 255),
						'email'			=> str_repeat('x', 64) . '@g.com',
						'status'		=> 1
					),
				),				
				array(				
					array(
						'id' 		    => '1',
						'firstName'	    => str_repeat('x', 255),
						'lastName'		=> str_repeat('x', 255),
						'address'	    => str_repeat('x', 500),
						'phoneWork'	    => str_repeat('2', 255),
						'phoneCel'	    => str_repeat('a', 255),
						'phoneHome'	    => str_repeat('2', 255),
						'rif'	        => str_repeat('x', 255),
						'fiscalName'    => str_repeat('x', 255),
						'companyName'   => str_repeat('x', 255),
						'email'			=> str_repeat('x', 64) . '@g.com',
						'status'		=> 1
					),
				),				
				array(				
					array(
						'id' 		    => '1',
						'firstName'	    => str_repeat('x', 255),
						'lastName'		=> str_repeat('x', 255),
						'address'	    => str_repeat('x', 500),
						'phoneWork'	    => str_repeat('2', 255),
						'phoneCel'	    => str_repeat('2', 255),
						'phoneHome'	    => str_repeat('a', 255),
						'rif'	        => str_repeat('x', 255),
						'fiscalName'    => str_repeat('x', 255),
						'companyName'   => str_repeat('x', 255),
						'email'			=> str_repeat('x', 64) . '@g.com',
						'status'		=> 1
					),
				),
				array(				
					array(
						'id' 		    => '1',
						'firstName'	    => str_repeat('x', 255),
						'lastName'		=> str_repeat('x', 255),
						'address'	    => str_repeat('x', 500),
						'phoneWork'	    => str_repeat('2', 255),
						'phoneCel'	    => str_repeat('2', 255),
						'phoneHome'	    => str_repeat('2', 255),
						'rif'	        => str_repeat('x', 300),
						'fiscalName'    => str_repeat('x', 255),
						'companyName'   => str_repeat('x', 255),
						'email'			=> str_repeat('x', 64) . '@g.com',
						'status'		=> 1
					),
				),
				array(				
					array(
						'id' 		    => '1',
						'firstName'	    => str_repeat('x', 255),
						'lastName'		=> str_repeat('x', 255),
						'address'	    => str_repeat('x', 500),
						'phoneWork'	    => str_repeat('2', 255),
						'phoneCel'	    => str_repeat('2', 255),
						'phoneHome'	    => str_repeat('2', 255),
						'rif'	        => null,
						'fiscalName'    => str_repeat('x', 255),
						'companyName'   => str_repeat('x', 255),
						'email'			=> str_repeat('x', 64) . '@g.com',
						'status'		=> 1
					),
				),
				array(				
					array(
						'id' 		    => '1',
						'firstName'	    => str_repeat('x', 255),
						'lastName'		=> str_repeat('x', 255),
						'address'	    => str_repeat('x', 500),
						'phoneWork'	    => str_repeat('2', 255),
						'phoneCel'	    => str_repeat('2', 255),
						'phoneHome'	    => str_repeat('2', 255),
						'rif'	        => "",
						'fiscalName'    => str_repeat('x', 255),
						'companyName'   => str_repeat('x', 255),
						'email'			=> str_repeat('x', 64) . '@g.com',
						'status'		=> 1
					),
				),
				array(				
					array(
						'id' 		    => '1',
						'firstName'	    => str_repeat('x', 255),
						'lastName'		=> str_repeat('x', 255),
						'address'	    => str_repeat('x', 500),
						'phoneWork'	    => str_repeat('2', 255),
						'phoneCel'	    => str_repeat('2', 255),
						'phoneHome'	    => str_repeat('2', 255),
						'rif'	        => str_repeat('a', 255),
						'fiscalName'    => str_repeat('x', 300),
						'companyName'   => str_repeat('x', 255),
						'email'			=> str_repeat('x', 64) . '@g.com',
						'status'		=> 1
					),
				),
				array(				
					array(
						'id' 		    => '1',
						'firstName'	    => str_repeat('x', 255),
						'lastName'		=> str_repeat('x', 255),
						'address'	    => str_repeat('x', 500),
						'phoneWork'	    => str_repeat('2', 255),
						'phoneCel'	    => str_repeat('2', 255),
						'phoneHome'	    => str_repeat('2', 255),
						'rif'	        => str_repeat('a', 255),
						'fiscalName'    => null,
						'companyName'   => str_repeat('x', 255),
						'email'			=> str_repeat('x', 64) . '@g.com',
						'status'		=> 1
					),
				),
				array(				
					array(
						'id' 		    => '1',
						'firstName'	    => str_repeat('x', 255),
						'lastName'		=> str_repeat('x', 255),
						'address'	    => str_repeat('x', 500),
						'phoneWork'	    => str_repeat('2', 255),
						'phoneCel'	    => str_repeat('2', 255),
						'phoneHome'	    => str_repeat('2', 255),
						'rif'	        => str_repeat('a', 255),
						'fiscalName'    => "",
						'companyName'   => str_repeat('x', 255),
						'email'			=> str_repeat('x', 64) . '@g.com',
						'status'		=> 1
					),
				),
				array(				
					array(
						'id' 		    => '1',
						'firstName'	    => str_repeat('x', 255),
						'lastName'		=> str_repeat('x', 255),
						'address'	    => str_repeat('x', 500),
						'phoneWork'	    => str_repeat('2', 255),
						'phoneCel'	    => str_repeat('2', 255),
						'phoneHome'	    => str_repeat('2', 255),
						'rif'	        => str_repeat('a', 255),
						'fiscalName'    => str_repeat('b', 255),
						'companyName'   => str_repeat('x', 300),
						'email'			=> str_repeat('x', 64) . '@g.com',
						'status'		=> 1
					),
				),
				array(				
					array(
						'id' 		    => '1',
						'firstName'	    => str_repeat('x', 255),
						'lastName'		=> str_repeat('x', 255),
						'address'	    => str_repeat('x', 500),
						'phoneWork'	    => str_repeat('2', 255),
						'phoneCel'	    => str_repeat('2', 255),
						'phoneHome'	    => str_repeat('2', 255),
						'rif'	        => str_repeat('a', 255),
						'fiscalName'    => str_repeat('b', 255),
						'companyName'   => str_repeat('x', 300),
						'email'			=> str_repeat('x', 64),
						'status'		=> 1
					),
				),
				array(				
					array(
						'id' 		    => '1',
						'firstName'	    => str_repeat('x', 255),
						'lastName'		=> str_repeat('x', 255),
						'address'	    => str_repeat('x', 500),
						'phoneWork'	    => str_repeat('2', 255),
						'phoneCel'	    => str_repeat('2', 255),
						'phoneHome'	    => str_repeat('2', 255),
						'rif'	        => str_repeat('a', 255),
						'fiscalName'    => str_repeat('b', 255),
						'companyName'   => str_repeat('x', 300),
						'email'			=> str_repeat('x', 64),
						'status'		=> "a"
					),
				),
			);
	}
}