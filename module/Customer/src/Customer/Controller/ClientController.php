<?php
/**
 * Copyright (c) 2010-2014, Soluciones Tecnologicas y Teleinformaticas Netware C.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Sistelnet C.A. nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 * @package    Customer
 * @author     David López <dlopez@sistelnet.com.ve>
 * @copyright  2010-2014 Sistelnet C.A. <dlopez@sistelnet.com.ve>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://www.sistelnet.com.ve
 */
namespace Customer\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Form\Element;
use Customer\Entity\Client;
use Doctrine\ORM\EntityManager;
use DoctrineORMModule\Form\Annotation\AnnotationBuilder;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;

/**
 * Client Controller
 *
 * @author David López <dlopez@sistelnet.com.ve>
 */
class ClientController extends AbstractActionController {
	/**             
	 * @var Doctrine\ORM\EntityManager
	 */                
	protected $em;

	/**
	 * Set Doctrine entity manager
	 * @param EntityManager $em [description]
	 */
	public function setEntityManager(EntityManager $em){
        $this->em = $em;
    }
 
    /**
     * Get doctriner entity manager
     * @return EntityManager [description]
     */
    public function getEntityManager(){
        if (null === $this->em) {
            $this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }
        return $this->em;
    } 
	
	/**
	 * (non-PHPdoc)
	 *
	 * @see \Zend\Mvc\Controller\AbstractActionController::indexAction()
	 */
	public function indexAction() {
		return new ViewModel ( array (
				'clients' => $this->getEntityManager()->getRepository('Customer\Entity\Client')->findAll()
		) );
	}
	
	/**
	 *
	 * @return Ambigous <\Zend\Http\Response, \Zend\Stdlib\ResponseInterface>|multitype:\Clientes\Controller\AlbumForm
	 */
	public function addAction(){
		$builder = new AnnotationBuilder($this->getEntityManager());
		$client = new Client();
		$form = $builder->createForm($client);

		$form->setHydrator(new DoctrineHydrator($this->getEntityManager(), 'Customer\Entity\Client'));
        $form->bind($client);

        $send = new Element('send');
        $send->setValue ('Create'); // submit
        $send->setAttributes(array('type' => 'submit' ));
        $form->add ($send);

		$request = $this->getRequest ();
		if ($request->isPost()){
			$form->setInputFilter($client->getInputFilter ());
			$form->setData ($request->getPost());
			
			if ($form->isValid()){
				$client->setOptions($form->getData());
				$this->getEntityManager()->persist($client);
				$this->getEntityManager()->flush();
				
				// Redirect to list of clientes
				return $this->redirect()->toRoute('client');
			}
		}
		return array (
			'form' => $form
		);
	}
}

