<?php
/**
 * Copyright (c) 2010-2014, Soluciones Tecnologicas y Teleinformaticas Netware C.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Sistelnet C.A. nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 * @package    Customer
 * @author     David López <dlopez@sistelnet.com.ve>
 * @copyright  2010-2014 Sistelnet C.A. <dlopez@sistelnet.com.ve>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://www.sistelnet.com.ve
 */
namespace Customer\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\Input;
use Zend\I18n\Validator;
use Zend\Form\Annotation;
use Customer\Exception;

/**
 * Client Class
 *
 * @ORM\Entity
 * @ORM\Table(name="client")
 * @Annotation\Name("Client")
 * @Annotation\Hydrator("Zend\Stdlib\Hydrator\ObjectProperty")
 * @author David López <dlopez@sistelnet.com.ve>
 */
class Client implements InputFilterAwareInterface{
	/**
	 * Type Rif Venezolano / Persona Natural
	 */
	const TYPE_RIF_V = 'V';

	/**
	 * Type Rif Juridico
	 */
	const TYPE_RIF_J = 'J';

	/**
	 * Type Rif Gubernamental
	 */
	const TYPE_RIG_G = 'G';

	/**
	 * Type Rif Extranejro
	 */
	const TYPE_RIF_E = 'E';
	
	/**
	 * Status for a new client
	 * @const integer
	 */
	const STATUS_CLIENT_NEW = 0;
	
	/**
	 * Status for active client
	 * @const integer
	 */
	const STATUS_CLIENT_ACTIVE = 1;
	
	/**
	 * Status for unactive client
	 * @const integer
	 */
	const STATUS_CLIENT_UNACTIVE = 2;

	/**
	 * Filter for var
	 *
	 * @Annotation\Exclude()
	 * @var Zend\InputFilter\Input
	 */
	protected $inputFilter;
	
	/**
	 * Client Id's
	 * 
	 * @Annotation\Exclude()
	 * @ORM\Id @ORM\Column(type="integer") @ORM\GeneratedValue
	 * @var int
	 */
	protected $id;
	
	/**
	 * First name client's
	 *
	 * @Annotation\Attributes({"type":"text"})
     * @Annotation\Options({"label":"Nombres"}) 
     * @Annotation\Attributes({"class":"form-control"})
	 * @ORM\Column(type="string", name="first_name")
	 * @var string
	 */
	protected $firstName;

	/**
	 * Last name client's
	 * 
	 * 
	 * @Annotation\Attributes({"type":"text"})
     * @Annotation\Options({"label":"Apellidos"})
     * @Annotation\Attributes({"class":"form-control"})
     * @ORM\Column(type="string", name="last_name")
	 * @var string
	 */
	protected $lastName;

	/**
	 * Fiscal name client's
	 * 
	 * @Annotation\Attributes({"type":"text"})
     * @Annotation\Options({"label":"Fiscal Name"})
     * @Annotation\Attributes({"class":"form-control"})
	 * @ORM\Column(type="string", name="fiscal_name")
	 * @var string
	 */
	protected $fiscalName;

	/**
	 * Company name belongs client
	 * 
	 * @Annotation\Attributes({"type":"text"})
     * @Annotation\Options({"label":"Compañia"})
     * @Annotation\Attributes({"class":"form-control"})
	 * @ORM\Column(type="string", name="company_name", nullable=true)
	 * @var string
	 */
	protected $companyName;
	
	/**
	 * Client Rif's
	 *
	 * @Annotation\Attributes({"type":"text"})
     * @Annotation\Options({"label":"Rif"})
     * @Annotation\Attributes({"class":"form-control"})
	 * @ORM\Column(type="string")
	 * @var string
	 */
	protected $rif;
	
	/**
	 * Client address's
	 *
	 * @Annotation\Attributes({"type":"text"})
     * @Annotation\Options({"label":"Direccion"})
     * @Annotation\Attributes({"class":"form-control"})
	 * @ORM\Column(type="text")
	 * @var string
	 */
	protected $address;

	/**
	 * Home phone numer client's
	 *
	 * @Annotation\Attributes({"type":"text"})
     * @Annotation\Options({"label":"Tel. Casa"})
     * @Annotation\Attributes({"class":"form-control"})
	 * @ORM\Column(type="string", name="phone_home")
	 * @var string
	 */
	protected $phoneHome;

	/**
	 * Work phone number client's
	 *
	 * @Annotation\Attributes({"type":"text"})
     * @Annotation\Options({"label":"Tel. Trabajo"})
     * @Annotation\Attributes({"class":"form-control"})
	 * @ORM\Column(type="string", name="phone_work", nullable=true)
	 * @var string
	 */
	protected $phoneWork;

	/**
	 * Mobile phone number client's
	 *
	 * @Annotation\Attributes({"type":"text"})
     * @Annotation\Options({"label":"Tel. Celular"})
     * @Annotation\Attributes({"class":"form-control"})
	 * @ORM\Column(type="string", name="phone_cel", nullable=true)
	 * @var string
	 */
	protected $phoneCel;

	/**
	 * Email client's
	 *
	 * @Annotation\Attributes({"type":"email"})
     * @Annotation\Options({"label":"Email"})
     * @Annotation\Attributes({"class":"form-control"})
	 * @ORM\Column(type="string", nullable=true)
	 * @var string
	 */
	protected $email;

	/**
	 * Status client's
	 *
	 * @Annotation\Exclude()
	 * @ORM\Column(type="smallint")
	 * @var integer
	 */
	protected $status;

	/**
	 * Timestamp when client was create
	 *
	 * @Annotation\Exclude()
	 * @ORM\Column(type="datetime")
	 * @var timestamp
	 */
	protected $tsCreate;

	/**
	 * Timestamp when client was update for last time
	 *
	 * @Annotation\Exclude()
	 * @ORM\Column(type="datetime", nullable=true)
	 * @var timestamp
	 */
	protected $tsUpdate;

	/**
	 * Client has contracts
	 * 
	 * @Annotation\Exclude()
	 * @ORM\OneToMany(targetEntity="Contract", mappedBy="id")
	 * @var Contract[]
	 */
	protected $contracts;
	
	/**
	 * Client Class's contructor
	 *
	 * @param array $data        	
	 */
	public function __construct(array $data = null) {
		if (is_array($data)) {
			$this->setOptions($data);
		}
		$this->contracts = new ArrayCollection();
	}
	
	/**
	 * Magic method for getters
	 *
	 * @param string $name        	
	 * @throws Exception\BadMethodCallException
	 */
	public function __get($name) {
		$method = 'get' . ucfirst ($name);
		
		if (! method_exists ($this, $method)) {
			throw new Exception\BadMethodCallException ('Property Invalid ' . $name);
		}
		return $this->$method();
	}
	
	/**
	 * Magic method for setter
	 *
	 * @param string $name Property name
	 * @param mixed $value Property value
	 * @throws Exception\BadMethodCallException
	 */
	public function __set($name, $value) {
		$method = 'set' . ucfirst ( $name );
		if (! method_exists ( $this, $method )) {
			throw new Exception\BadMethodCallException ('Property Invalid ' . $name);
		}
		$this->$method($value);
	}
	
	/**
	 * Given an array exchanges data with property
	 *
	 * @param array $data Associative array with property
	 * @throws Exception\BadMethodCallException
	 */
	public function setOptions($data) {
		$methods = get_class_methods($this);
        foreach($data as $key => $value)
        {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            } else {
            	throw new Exception\BadMethodCallException ('Property Invalid ' . $key);
            }
        }
        return $this;
	}
	
	/**
	 * Get id client's
	 * 
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}
	
	/**
	 * Set id client's
	 *
	 * @param int $id
	 * @return void
	 * @throws Exception\InvalidArgumentException
	 */
	public function setId($id) {
		$input = $this->getInputFilter()->get('id');
		$input->setValue($id);
		if (! $input->isValid())
			throw new Exception\InvalidArgumentException( "Invalid value for Client Id " . print_r($input->getMessages(), true));
		$this->id = $input->getValue();
	}
	
	/**
	 * Get First Name Client's
	 *
	 * @return string
	 */
	public function getFirstName() {
		return $this->firstName;
	}
	
	/**
	 * Set First Name Client's
	 *
	 * @param string $firstName
	 * @throws Exception\InvalidArgumentException
	 */
	public function setFirstName($firstName) {
		$input = $this->getInputFilter()->get('firstName');

		$input->setValue($firstName);
		if (!$input->isValid())
			throw new Exception\InvalidArgumentException( "Invalid value for Client First Name " . print_r($input->getMessages(), true));
		$this->firstName = $input->getValue();
	}

	/**
	 * Get Last Name Client's
	 *
	 * @return string
	 */
	public function getLastName() {
		return $this->lastName;
	}
	
	/**
	 * Set Last Name Client's
	 *
	 * @param string $lastName
	 * @throws Exception\InvalidArgumentException
	 */
	public function setLastName($lastName) {
		$input = $this->getInputFilter()->get('lastName');

		$input->setValue($lastName);
		if (!$input->isValid())
			throw new Exception\InvalidArgumentException( "Invalid value for Client Last Name " . print_r($input->getMessages(), true));
		$this->lastName = $input->getValue();
	}

	/**
	 * Get Company Name Client's
	 *
	 * @return string
	 */
	public function getCompanyName() {
		return $this->companyName;
	}
	
	/**
	 * Set Last Name Client's
	 *
	 * @param string $companyName
	 * @throws Exception\InvalidArgumentException
	 */
	public function setCompanyName($companyName) {
		$input = $this->getInputFilter()->get('companyName');

		$input->setValue($companyName);
		if (!$input->isValid())
			throw new Exception\InvalidArgumentException( "Invalid value for Client Company Name " . print_r($input->getMessages(), true));
		$this->companyName = $input->getValue();
	}

	/**
	 * Get phone home Client's
	 *
	 * @return string
	 */
	public function getPhoneHome() {
		return $this->phoneHome;
	}
	
	/**
	 * Set phone home Client's
	 *
	 * @param string $phoneHome
	 * @throws Exception\InvalidArgumentException
	 */
	public function setPhoneHome($phoneHome) {
		$input = $this->getInputFilter()->get('phoneHome');

		$input->setValue($phoneHome);
		if (!$input->isValid())
			throw new Exception\InvalidArgumentException( "Invalid value for Client phone home " . print_r($input->getMessages(), true));
		$this->phoneHome = $input->getValue();
	}

	/**
	 * Get phone work Client's
	 *
	 * @return string
	 */
	public function getPhoneWork() {
		return $this->phoneWork;
	}
	
	/**
	 * Set phone work Client's
	 *
	 * @param string $phoneWork
	 * @throws Exception\InvalidArgumentException
	 */
	public function setPhoneWork($phoneWork) {
		$input = $this->getInputFilter()->get('phoneWork');

		$input->setValue($phoneWork);
		if (!$input->isValid())
			throw new Exception\InvalidArgumentException( "Invalid value for Client phone work " . print_r($input->getMessages(), true));
		$this->phoneWork = $input->getValue();
	}

	/**
	 * Get phone cel Client's
	 *
	 * @return string
	 */
	public function getPhoneCel() {
		return $this->phoneCel;
	}
	
	/**
	 * Set phone home Client's
	 *
	 * @param string $phoneCel
	 * @throws Exception\InvalidArgumentException
	 */
	public function setPhoneCel($phoneCel) {
		$input = $this->getInputFilter()->get('phoneCel');

		$input->setValue($phoneCel);
		if (!$input->isValid())
			throw new Exception\InvalidArgumentException( "Invalid value for Client phone cel " . print_r($input->getMessages(), true));
		$this->phoneCel = $input->getValue();
	}

	/**
	 * Get Rif Client's
	 *
	 * @return string
	 */
	public function getRif() {
		return $this->rif;
	}
	
	/**
	 * Set rif client's
	 *
	 * @param string $rif
	 * @throws Exception\InvalidArgumentException
	 */
	public function setRif($rif) {
		$input = $this->getInputFilter()->get('rif');

		$input->setValue($rif);
		if (!$input->isValid())
			throw new Exception\InvalidArgumentException( "Invalid value for Client Rif " . print_r($input->getMessages(), true));
		$this->rif = $input->getValue();
	}
	
	/**
	 * Get name client's
	 *
	 * @return string
	 */
	public function getName() {
		return sprintf("%s %s", $this->firstName, $this->lastName);
	}
	
	/**
	 * Get address client's
	 *
	 * @return string
	 */
	public function getAddress() {
		return $this->address;
	}
	
	/**
	 * Set address client's
	 *
	 * @param string $address
	 * @return void
	 */
	public function setAddress($address) {
		$input = $this->getInputFilter()->get('address');

		$input->setValue($address);
		if (!$input->isValid())
			throw new Exception\InvalidArgumentException( "Invalid value for Client address " . print_r($input->getMessages(), true));
		$this->address = $input->getValue();
	}
	
	/**
	 * Get fiscal name client's
	 *
	 * @return string
	 */
	public function getFiscalName() {
		return $this->fiscalName;
	}
	
	/**
	 * Set fiscal name client's
	 *
	 * @param string $nameFiscal
	 * @return void
	 */
	public function setFiscalName($fiscalName) {
		$input = $this->getInputFilter()->get('fiscalName');

		$input->setValue($fiscalName);
		if (!$input->isValid())
			throw new Exception\InvalidArgumentException( "Invalid value for Client Fiscal Name " . print_r($input->getMessages(), true));
		$this->fiscalName = $input->getValue();
	}

	/**
	 * Get email address client's
	 *
	 * @return string
	 */
	public function getEmail() {
		return $this->email;
	}
	
	/**
	 * Set email address client's
	 *
	 * @param string $email
	 * @return void
	 */
	public function setEmail($email) {
		$inputFilter = $this->getInputFilter()->get('email');

		$inputFilter->setValue($email);
		if (!$inputFilter->isValid())
			throw new Exception\InvalidArgumentException( "Invalid value for Client Email " . print_r($inputFilter->getMessages(), true));
		$this->email = $inputFilter->getValue();
	}

	/**
	 * Get status client's
	 *
	 * @return int
	 */
	public function getStatus() {
		return $this->status;
	}
	
	/**
	 * Set status client's
	 *
	 * @param int $status
	 * @return void
	 */
	public function setStatus($status) {
		$inputFilter = $this->getInputFilter()->get('status');

		$inputFilter->setValue($status);
		if (!$inputFilter->isValid())
			throw new Exception\InvalidArgumentException( "Invalid value for Client Status " . print_r($inputFilter->getMessages(), true));
		$this->status = $inputFilter->getValue();
	}

	/**
	 * Get timestamp when client is created
	 *
	 * @return DateTime
	 */
	public function getTsCreate() {
		return $this->tsCreate;
	}
	
	/**
	 * Set timestamp when client is created
	 *
	 * @param DateTime $tsCreate
	 * @return void
	 */
	public function setTsCreate($tsCreate) {
		$inputFilter = $this->getInputFilter()->get('tsCreate');

		$inputFilter->setValue($tsCreate);
		if (!$inputFilter->isValid())
			throw new Exception\InvalidArgumentException( "Invalid value for Client tsCreate " . print_r($inputFilter->getMessages(), true));
		$this->tsCreate = $inputFilter->getValue();
	}

	/**
	 * Get timestamp when client was update
	 *
	 * @return DateTime
	 */
	public function getTsUpdate() {
		return $this->tsUpdate;
	}
	
	/**
	 * Set timestamp when client was update
	 *
	 * @param DateTime $tsUpdate
	 * @return void
	 */
	public function setTsUpdate($tsUpdate) {
		$inputFilter = $this->getInputFilter()->get('tsUpdate');

		$inputFilter->setValue($tsUpdate);
		if (!$inputFilter->isValid())
			throw new Exception\InvalidArgumentException( "Invalid value for Client tsUpdate " . print_r($inputFilter->getMessages(), true));
		$this->tsUpdate = $inputFilter->getValue();
	}

	/**
	 * Add a contract to client
	 * @param Contract $contract
	 */
	public function addContract(Contract $contract){
		$this->contracts->add($contract);
	}

	/**
	 * Get all contract for this client
	 * @return ArrayColletion
	 */
	public function getContracts(){
		return $this->contracts;
	}
	
	/**
	 *
	 * @param InputFilterInterface $inputFilter        	
	 * @throws Exception\BadMethodCallException
	 */
	public function setInputFilter(InputFilterInterface $inputFilter) {
		throw new Exception\BadMethodCallException("Not used");
	}
	
	/**
	 * (non-PHPdoc)
	 *
	 * @see \Zend\InputFilter\InputFilterAwareInterface::getInputFilter()
	 * @return InputFilter
	 */
	public function getInputFilter() {
		if (! $this->inputFilter) {
			$inputFilter = new InputFilter();
			
			$idInput = new Input('id');
			$idInput->getValidatorChain()
			        ->attach(new Validator\Int());
			$inputFilter->add($idInput);

			$statusInput = new Input('status');
			$statusInput->getValidatorChain()
			        ->attach(new Validator\Int());
			$inputFilter->add($statusInput);
			
			$inputFilter->add ( array (
				'name' => 'firstName',
				'required' => true,
				'filters' => array (
					array (
						'name' => 'StripTags' 
					),
					array (
						'name' => 'StringTrim' 
					),
					array (
						'name' => 'StringToUpper' 
					)
				),
				'validators' => array (
					array (
						'name' => 'StringLength',
						'options' => array (
							'encoding' => 'UTF-8',
							'min'      => 1,
							'max'      => 255
						) 
					),
					array (
						'name' => 'Alpha',
						'options' => array (
							'allowWhiteSpace' => true,
						) 
					),
				) 
			));

			$inputFilter->add ( array (
				'name' => 'lastName',
				'required' => true,
				'filters' => array (
					array (
						'name' => 'StripTags' 
					),
					array (
						'name' => 'StringTrim' 
					),
					array (
						'name' => 'StringToUpper' 
					)
				),
				'validators' => array (
					array (
						'name' => 'StringLength',
						'options' => array (
							'encoding' => 'UTF-8',
							'min' => 1,
							'max' => 255
						) 
					),
					array (
						'name' => 'Alpha',
						'options' => array (
							'allowWhiteSpace' => true,
						) 
					),
				) 
			));
			
			$inputFilter->add ( array (
				'name' => 'address',
				'required' => true,
				'filters' => array (
					array (
						'name' => 'StripTags' 
					),
					array (
						'name' => 'StringTrim' 
					),
					array (
						'name' => 'StringToUpper' 
					)
				),
			));

			$inputFilter->add ( array(
				'name'     => 'phoneHome',
				'required' => true,
				'filters'  => array (
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
					array('name' => 'Digits'),
					array(
						'name' => 'StringLength',
						'options' => array(
							'min' => 1,
							'max' => 255 
						) 
					)
				) 
			));

			$inputFilter->add ( array(
				'name'     => 'phoneWork',
				'required' => false,
				'filters'  => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
					array('name' => 'Digits'),
					array(
						'name' => 'StringLength',
						'options' => array(
							'min' => 11,
							'max' => 255 
						) 
					)
				) 
			));

			$inputFilter->add ( array(
				'name'     => 'phoneCel',				
				'required' => false,
				'filters'  => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
					array('name' => 'Digits'),
					array(
						'name' => 'StringLength',
						'options' => array(
							'min' => 11,
							'max' => 255 
						) 
					)
				) 
			));

			$inputFilter->add(array(
				'name' => 'rif',
				'required' => true,
				'filters' => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
					array('name' => 'StringToUpper'),
				),
				'validators' => array(
					array(
						'name' => 'StringLength',
						'options' => array(
							'encoding' => 'UTF-8',
							'min' => 1,
							'max' => 255 
						) 
					),
				) 
			));

			$inputFilter->add(array(
				'name' => 'fiscalName',
				'required' => true,
				'filters' => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
					array('name' => 'StringToUpper'),
				),
				'validators' => array(
					array(
						'name' => 'StringLength',
						'options' => array(
							'encoding' => 'UTF-8',
							'min'      => 1,
							'max'      => 255 
						) 
					),
				) 
			));

			$inputFilter->add(array(
				'name'     => 'companyName',
				'required' => false,
				'filters'  => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
					array('name' => 'StringToUpper'),
				),
				'validators' => array(
					array(
						'name' => 'StringLength',
						'options' => array(
							'encoding' => 'UTF-8',
							'max'      => 255 
						) 
					),
				) 
			));

			$inputFilter->add(array(
				'name' => 'email',				
				'required' => false,
				'filters' => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
					array('name' => 'StringToUpper'),
				),
				'validators' => array(
					array('name'    => 'EmailAddress'),
				) 
			));
			
			$inputFilter->add(array(
				'name' => 'tsCreate',
				'validators' => array(
					array(
						'name'    => 'Date',
						'options' => array(
							'format' => "Y-m-d H:i:s"
						)
					),
				) 
			));

			$inputFilter->add(array(
				'name' => 'tsUpdate',
				'validators' => array(
					array(
						'name'    => 'Date',
						'options' => array(
							'format' => "Y-m-d H:i:s"
						)
					),
				) 
			));

			$this->inputFilter = $inputFilter;
		}
		
		return $this->inputFilter;
	}
	/**
	 * Devuelve todos las claves de la variables constantes de la clase
	 *
	 * @static
	 * @return array
	 */
	static function getAllByConstant($token = null) {
		if (is_null ( $token ) === true) {
			return null;
		} elseif (is_string ( $token ) === true) {
			$token = strtoupper ( $token );
			if (strpos ( $token, '_' ) === false) {
				$token .= "_";
			}
			$oClass = new \ReflectionClass ('Customer\Entity\Client');
			$status = $oClass->getConstants ();
			$estatus = array ();
			foreach ( $status as $key => $value ) {
				if (strpos ( $key, $token ) !== false) {
					$estatus [$value] = str_replace ( "_", " ", substr ( $key, strlen ( $token ) ) );
					strtok ( $key, $token );
				}
			}
			return $estatus;
		} else {
			throw new \Exception ( "It expectd a string" );
		}
	}
	
	/**
	 * Devuelve el nombre de la constante solicitada
	 *
	 * @static
	 *
	 * @param $token Tipo
	 *        	de constante que se quiere
	 * @param $key Valor
	 *        	de la constante
	 * @return array
	 */
	static function getNombreConstant($token, $key) {
		$constantes = Clientes_Model_Cliente::getAllByConstant ( $token );
		return $constantes [$key];
	}
}