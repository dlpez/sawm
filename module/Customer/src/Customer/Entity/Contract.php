<?php
namespace Customer\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\Input;
use Zend\Form\Annotation;
/**
 * Class Contract
 * 
 * Objecto for Contract Client's
 * 
 * @ORM\Entity
 * @ORM\Table(name="Contract")
 * @Annotation\Name("Contract")
 * @Annotation\Hydrator("Zend\Stdlib\Hydrator\ObjectProperty")
 * @copyright  	Copyright (c) 2006-2012 SISTELNET C.A.
 * @license    	New BSD License
 * @version		$Id:$e
 * @author		David Lopez <dlopez@sistelnet.com.ve>
 */
class Contract
{
    /**
     * Estado para un contrato firmado, cuando se firma el contrato y se espera por la instalacion.
     * @const integer
     */
    const STATUS_FIRMADO = 0;

    /**
     * STATUS para un contrato cerrado, es un contrato activo.
     * @const integer
     */
    const STATUS_ACTIVO = 1;

    /**
     * STATUS cuando un contrato es cancelado antes de su instalacion.
     * @const integer
     */
    const STATUS_CANCELADO = 2;

    /**
     * STATUS para una un contrato finalizado
     * @const integer
     */
    const STATUS_FINALIZADO = 3;

    /**
     * Filter for var
     * @var Zend\InputFilter\Input
     * @Annotation\Exclude()
     */
    protected $inputFilter;

    /**
     * Contract Id's
     * @ORM\Id @ORM\Column(type="integer") @ORM\GeneratedValue
     * @Annotation\Exclude()
     * @var int
     */
	protected $id;

    /**
     * Contract number's
     * @var string
     * @ORM\Column @ORM\Column(type="string")
     * @Annotation\Exclude()
     */
    protected $number;

    /**
     * Date when contract signed
     * @var date
     * @ORM\Column @ORM\Column(type="date")
     * @Annotation\Exclude()
     */
    protected $date;

    /**
     * Client owner contract's
     * @var Client
     * @ORM\ManyToOne(targetEntity="Client", inversedBy="contracts")
     * @Annotation\Exclude()
     */
    protected $client;

	/**
	 * Indicamos el estado del Contrato
     * @ORM\Column @ORM\Column(type="smallint")
     * @Annotation\Type("Zend\Form\Element\Select")
     * @Annotation\Options({"label":"Status:"})
     * @Annotation\Attributes({"class":"form-control"})
	 * @var int
	 */
	protected $status;

	/**
	 * Indicamos la nota del Contrato
     * @ORM\Column @ORM\Column(type="text")
     * @Annotation\Type("Zend\Form\Element\Textarea")
     * @Annotation\Options({"label":"Notes:"})
     * @Annotation\Attributes({"class":"form-control"})
	 * @var string
	 */
	protected $note;

    /**
     * Constructor de la Clase Redex_Model_Equipo
     * 
     * @param array $options Crear un contrato con $options; defecto: null
     */
    public function __construct(array $options =null)
    {
        if(is_array($options))
        {
            $this->setOptions($options);
        }
    }

	/**
     * Definir un atributo de la clase
     * 
     * @var string $name Nombre del atributo
     * @var string $value Valor del atributo
     * @return void
     */
    public function __set($name, $value)
    {
        $method = 'set' . ucfirst($name);
        if(('mapper' == $name) || !method_exists($this, $method))
        {
            throw new \Exception('Propiedad del contrato invalido');
        }
        $this->$method($value);
    }

	/**
     * Obtener un atributo de la clase
     * 
     * @param string $name
     * @return mixed Valor del atributo
     */
    public function __get($name)
    {
        $method = 'get' . ucfirst($name);
        if(('mapper' == $name) || !method_exists($this, $method))
        {
            throw new \Exception('Propiedad del contrato invalido ' . $name);
        }
        return $this->$method();
    }

	 /**
     * Define los atributos de la clase por medio de opciones
     * 
     * @param array $options Lista de opciones
     * @return Contract
     */
    public function setOptions(array $options)
    {
        $methods = get_class_methods($this);
        foreach($options as $key => $value)
        {
            $method = 'set' . ucfirst($key);
            if(in_array($method, $methods))
            {
                $this->$method($value);
            }
        }
        return $this;
    }

	/**
	 * Obtener el numero de Contrato, lo devuelve con el formato 000000
	 * @return integer
	 */
	public function getNumber(){
	    if (empty($this->number) && !empty($this->id)){
            $this->number = sprintf("%06d",$this->id);
        }
        return $this->number;
	}

    /**
     * Obtener el estado del contrato
     * @return string
     */
    public function getStatus(){
        return $this->status;
    }

    /**
     * Obtener la fecha del contrato
     * @return date
     */
    public function getDate(){
        return $this->date;
    }

    /**
     * Obtener la nota del contrato
     * @return string
     */
    public function getNote(){
        return $this->note;
    }

    /**
     * Get id contract's
     * @return int
     */
    public function getId(){
        return $this->id;
    }

    /**
     * Get client owner for this contract
     * @return Client
     */
    public function getClient(){
        return $this->client;
    }
	
    /**
     * Set id to contract
     * @param int $id
     */
    public function setId($id){
        $inputFilId = $this->getInputFilter()->get('id');
        $data = array (
            'id' => $id
        );
        $inputFilId->setData($data);
        if (! $inputFilId->isValid())
            throw new \Exception("Invalid data for contrat id");
        $this->id = $inputFilId->getValue ('id');
    }

    /**
	 * Definir el numero del contrato
	 * @param int $number
	 * @return void
	 */
	public function setNumber($number)
	{
		$inputFilId = $this->getInputFilter()->get('number');
        $data = array (
            'number' => $number
        );
        $inputFilId->setData($data);
        if (!$inputFilId->isValid())
            throw new \Exception ("Invalid data for number on contract");
        $this->number = $inputFilId->getValue('number');
	}

	/**
	 * Definir el Estado del contrato
	 * @param int $status
	 * @return void
	 */
	public function setStatus($status)
	{
		$inputFilId = $this->getInputFilter()->get('status');
        $data = array (
            'status' => $status
        );
        $inputFilId->setData($data);
        if (!$inputFilId->isValid())
            throw new \Exception ("Invalid data for status on contract");
        $this->status = $inputFilId->getValue('status');
	}
	
	/**
	 * Definir la fecha del contrato
	 * @param date $date fecha del contrato
	 * @return void
	 */
	public function setDate($date)
	{
		$inputFilId = $this->getInputFilter()->get('date');
        $data = array (
            'date' => $date
        );
        $inputFilId->setData($data);
        if (!$inputFilId->isValid())
            throw new \Exception ("Invalid data for date on contract");
        $this->date = $inputFilId->getValue('date');
	}
	
	
	/**
	 * Definir la nota del contrato
	 * @param string $note nota del contrato
	 * @return void
	 */
	public function setNote($note)
	{
		$inputFilId = $this->getInputFilter()->get('note');
        $data = array (
            'note' => $note
        );
        $inputFilId->setData($data);
        if (!$inputFilId->isValid())
            throw new \Exception ("Invalid data for note on contract");
        $this->note = $inputFilId->getValue('note');
	}

    /**
     * Set client owner contract
     * @param Client $client
     */
    public function setClient(Client $client){
        $this->client = $client;
    }

    /**
     * Obtener todos los estatus de un contrato
     * @return Array
     * @author Lenin Pernia <leninpernia@gmail.com>
     */
    static function getAllEstados(){
        $oClass = new ReflectionClass ('Contract');
        $oClass->getConstants();
        $status = $oClass->getConstants();
        $estatus = array();

        foreach($status as $key => $value){
            if(strpos($key, 'STATUS_') === false){
                
            }
            else
            {
                $estatus[$value] = substr($key, 7);
            }
        }
        return $estatus;
    }   

    /**
     * Obtener el nombre de un ESTATUS de un contrato
     * @param $key status
     * @return Array
     * @author Lenin Pernia <leninpernia@gmail.com>
     */
    static function getNombreEstado($key){
        $valores = self::getAllEstados();   
        return $valores[$key];
    }

    /**
     * (non-PHPdoc)
     *
     * @see \Zend\InputFilter\InputFilterAwareInterface::getInputFilter()
     * @return InputFilter
     */
    public function getInputFilter() {
        if (! $this->inputFilter) {
            $inputFilter = new InputFilter ();
            
            $inputFilter->add ( array (
                'name' => 'id',
                'filters' => array (
                    array (
                        'name' => 'Int' 
                    ) 
                ),
                'validators' => array (
                    array (
                        'name' => 'Int' 
                    ) 
                ) 
            ) );
            
            $inputFilter->add ( array (
                'name' => 'number',
                'required' => true,
                'filters' => array (
                    array (
                        'name' => 'StripTags' 
                    ),
                    array (
                        'name' => 'StringTrim' 
                    ) 
                ),
                'validators' => array (
                    array (
                        'name' => 'StringLength',
                        'options' => array (
                                'encoding' => 'UTF-8',
                                'min' => 1,
                                'max' => 255 
                        ) 
                    ) 
                ) 
            ) );
            
            $inputFilter->add ( array (
                'name' => 'date',
                'required' => true,
                'validators' => array (
                    array (
                        'name' => 'Date',
                    ) 
                ),
            ) );
            
            $inputFilter->add ( array (
                'name' => 'note',
                'filters' => array (
                    array (
                        'name' => 'StripTags' 
                    ),
                    array (
                        'name' => 'StringTrim' 
                    ),
                    array(
                        'note' => 'StringToUpper'
                    ),
                ),
            ) );
            
            $this->inputFilter = $inputFilter;
        }
        
        return $this->inputFilter;
    }
    
}