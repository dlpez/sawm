<?php

/**
 * 
 * @author dlopez
 * @since	beta
 */
namespace Customer\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Customer\Form\ClientForm;
use Customer\Entity\Client;
use Doctrine\ORM\EntityManager;

class ClientController extends AbstractActionController {
	/**             
	 * @var Doctrine\ORM\EntityManager
	 */                
	protected $em;
	 
	public function setEntityManager(EntityManager $em)
    {
        $this->em = $em;
    }
 
    public function getEntityManager()
    {
        if (null === $this->em) {
            $this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }
        return $this->em;
    } 
	
	/**
	 *
	 * @return Ambigous <object, multitype:, \Album\Model\AlbumTable>
	 */
	public function getClienteTable() {
		if (! $this->clienteTable) {
			$sm = $this->getServiceLocator ();
			$this->clienteTable = $sm->get ( 'Clientes\Model\ClienteTable' );
		}
		return $this->clienteTable;
	}
	
	/**
	 * (non-PHPdoc)
	 *
	 * @see \Zend\Mvc\Controller\AbstractActionController::indexAction()
	 */
	public function indexAction() {
		return new ViewModel ( array (
				'clients' => $this->getEntityManager()->getRepository('Customer\Entity\Client')->findAll()
		) );
	}
	
	/**
	 *
	 * @return Ambigous <\Zend\Http\Response, \Zend\Stdlib\ResponseInterface>|multitype:\Clientes\Controller\AlbumForm
	 */
	public function addAction(){
		$form = new ClientForm ();
		$form->get ('submit')->setValue ('Agregar');
		
		$request = $this->getRequest ();
		if ($request->isPost()){
			$cliente = new Client();
			$form->setInputFilter($cliente->getInputFilter ());
			$form->setData ($request->getPost ());
			
			if ($form->isValid()){
				$cliente->exchangeArray($form->getData());
				$this->getEntityManager()->persist($cliente);
				$this->getEntityManager()->flush();
				
				// Redirect to list of clientes
				return $this->redirect()->toRoute('client');
			}
		}
		return array (
				'form' => $form 
		);
	}
}

