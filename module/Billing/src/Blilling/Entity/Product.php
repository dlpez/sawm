<?php

class Product {

	protected $id;

	protected $name;

	protected $description;

	protected $hidden;

	protected $priceOnce;

	protected $priceMonthly;

	protected $priceQuarterly;

	protected $priceAnnual;

	protected $priceAnnual;

	protected $priceBiannual;

	protected $rateDown;

	protected $ceilDown;

	protected $rateUp;

	protected $ceilUp;

	protected $burstDown;

	protected $maxDownload;

	protected $maxUpload;

	protected $dueDayFirst;

	protected $dueDaySecond;

	protected $dueInterestFirst;

	protected $dueInterestSecond;

	protected $isRecurrently;

	// association with ProductGroup class
	public $=array();
	// association with InvoiceItem class
	public $=array();
	// association with RadGroupReply class
	public $=array();
	// association with RadGroupCheck class
	public $=array();
	// association with RadUserGroup class
	public $=array();
	// association with Contract class
	public $=array();

}


?>
