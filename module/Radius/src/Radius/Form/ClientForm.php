<?php

/**
 * 
 * @author dlopez
 *
 */
namespace Customer\Form;

use Zend\Form\Form;
use Customer\Entity\Client;

class ClientForm extends Form {
	public function __construct($name = null) {
		// we want to ignore the name passed
		parent::__construct ( 'client' );
		
		$this->add ( array (
				'name' => 'id',
				'type' => 'Hidden' 
		) );
		$this->add ( array (
				'name' => 'name',
				'type' => 'Text',
				'options' => array (
						'label' => 'Nombre' 
				),
				'attributes' => array (
						'class' => 'form-control',
						'placeholder' => 'Nombre' 
				) 
		) );
		$this->add ( array (
				'name' => 'typeRif',
				'type' => 'Select',
				'options' => array (
						'label' => 'Tipo Rif',
						'value_options' => Client::getAllByConstant ( 'TYPE_RIF' ) 
				),
				'attributes' => array (
						'class' => 'form-control' 
				) 
		) );
		$this->add ( array (
				'name' => 'rif',
				'type' => 'Text',
				'options' => array (
						'label' => 'Rif' 
				),
				'attributes' => array (
						'class' => 'form-control',
						'placeholder' => 'Rif' 
				) 
		) );
		$this->add ( array (
				'name' => 'fiscalName',
				'type' => 'Text',
				'options' => array (
						'label' => 'Razon Social' 
				),
				'attributes' => array (
						'class' => 'form-control',
						'placeholder' => 'Razon Social' 
				) 
		) );
		$this->add ( array (
				'name' => 'address',
				'type' => 'Textarea',
				'options' => array (
						'label' => 'Direccion' 
				),
				'attributes' => array (
						'class' => 'form-control',
						'placeholder' => 'Direccion',
						'rows' => '3' 
				) 
		) );
		$this->add ( array (
				'name' => 'submit',
				'type' => 'Submit',
				'attributes' => array (
						'value' => 'Go',
						'id' => 'submitbutton',
						'class' => 'btn btn-default' 
				) 
		) );
	}
}