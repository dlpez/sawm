<?php
namespace Customer\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Form\Element;
use Customer\Entity\Contract;
use Doctrine\ORM\EntityManager;
use DoctrineORMModule\Form\Annotation\AnnotationBuilder;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;

/**
 * 
 */
class ContractController extends AbstractActionController {
	/**             
	 * @var Doctrine\ORM\EntityManager
	 */                
	protected $em;
	 
	public function setEntityManager(EntityManager $em)
    {
        $this->em = $em;
    }
 
    public function getEntityManager()
    {
        if (null === $this->em) {
            $this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }
        return $this->em;
    }
	
	/**
	 * (non-PHPdoc)
	 *
	 * @see \Zend\Mvc\Controller\AbstractActionController::indexAction()
	 */
	public function indexAction() {
		return new ViewModel ( array (
				'contracts' => $this->getEntityManager()->getRepository('Customer\Entity\Contract')->findAll()
		) );
	}
	
	/**
	 *
	 * @return Ambigous <\Zend\Http\Response, \Zend\Stdlib\ResponseInterface>|multitype:\Clientes\Controller\AlbumForm
	 */
	public function addAction(){
		$builder = new AnnotationBuilder($this->getEntityManager());
		$contract = new Contract();
		$form = $builder->createForm($contract);

		$form->setHydrator(new DoctrineHydrator($this->getEntityManager(), 'Customer\Entity\Contract'));
        $form->bind ($contract); 

        $send = new Element('send');
        $send->setValue ('Create'); // submit
        $send->setAttributes(array('type' => 'submit' ));
        $form->add ($send);
		$request = $this->getRequest ();
		if ($request->isPost()){
			$cliente = new Client();
			$form->setInputFilter($cliente->getInputFilter ());
			$form->setData ($request->getPost ());
			
			if ($form->isValid()){
				$cliente->exchangeArray($form->getData());
				$this->getEntityManager()->persist($cliente);
				$this->getEntityManager()->flush();
				
				// Redirect to list of clientes
				return $this->redirect()->toRoute('client');
			}
		}
		return array (
			'form' => $form
		);
	}
}

