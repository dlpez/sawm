System Admin Wisp Managment
=======================

Introduction
------------
This is a proyect develop by Sistelnet C.A. For WISP automate Billing, Support and
Monitoring. It's develop with Zend Framework and Doctrine 2. Also has support for
MySQL

Please follow git develop schema
------------
See this image for more info [git](https://bitbucket.org/dlpez/sawm/wiki/Git_Plan)

Installation
------------
You find schema on data/sql.

Clone repo
------------

Please follow this [steps](https://confluence.atlassian.com/display/BITBUCKET/Clone+Your+Git+Repository+and+Add+Source+Files)

Web Server Setup
----------------

### PHP CLI Server

The simplest way to get started if you are using PHP 5.4 or above is to start the internal PHP cli-server in the root directory:

    php -S 0.0.0.0:8080 -t public/ public/index.php

This will start the cli-server on port 8080, and bind it to all network
interfaces.

**Note: ** The built-in CLI server is *for development only*.

### Apache Setup

To setup apache, setup a virtual host to point to the public/ directory of the
project and you should be ready to go! It should look something like below:

    <VirtualHost *:80>
        ServerName sawm.localhost
        DocumentRoot /path/to/sawm/public
        SetEnv APPLICATION_ENV "development"
        <Directory /path/to/sawm/public>
            DirectoryIndex index.php
            AllowOverride All
            Order allow,deny
            Allow from all
        </Directory>
    </VirtualHost>